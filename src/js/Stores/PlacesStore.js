/**
 * Created by luardo on 16/10/16.
 */
import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class PlacesStore extends EventEmitter {

    constructor() {
        super();

        var favorites = [];

        if(localStorage.favorites) {
            favorites = JSON.parse(localStorage.favorites);
        }

        this.favorites = favorites;
    }

    getAll() {
       return this.favorites;
    }

    add(address) {
        var favorites = this.getAll();

        favorites.push({
            address: address,
            timestamp: Date.now()
        });


        this.favorites = favorites;

        localStorage.favorites = JSON.stringify(favorites);

        this.emit("change");
    }

    remove(address) {
        var favorites = this.getAll();
        var index = -1;

        for(var i = 0; i < favorites.length; i++){

            if(favorites[i].address == address){
                index = i;
                break;
            }

        }

        // If it was found, remove it from the favorites array

        if(index !== -1){

            favorites.splice(index, 1);

            this.favorites = favorites;


            localStorage.favorites = JSON.stringify(favorites);
        }
    }

    handleActions(action) {

        switch (action.type) {
            case "ADD_LOCATION":
                this.addToFavorites(action.address);
                break;

            case "REMOVE_LOCATION":
                this.removeFavorite(action.address);
                break;
        }
    }

}

const placesStore = new PlacesStore;

dispatcher.register(placesStore.handleActions.bind(placesStore));

export default placesStore;