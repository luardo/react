import {EventEmitter} from "events";
import dispatcher from "../dispatcher"

class PostStore extends EventEmitter {
    constructor() {
        super();
        this.posts = [
                {
                    id: 8383838187,
                    title: "Man must must explore, and this is exploration at its greatest",
                    published: true
                },
                {
                    id: 223431131,
                    title: "I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.",
                    published: true
                }
            ];
    }

    createPost(title) {
        const id = Date.now();

        this.posts.push({
            id,
            title,
            published: true
        });

        this.emit("change");
    }

    getAll() {
        return this.posts;
    }

    handleActions(action) {

        switch (action.type) {
            case "CREATE_POST": {
                this.createPost(action.title);
            }
        }
    }

}

const postStore = new PostStore;

dispatcher.register(postStore.handleActions.bind(postStore));

window.postStore = postStore;
window.dispatcher = dispatcher;

export default postStore;
