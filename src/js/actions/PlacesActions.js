/**
 * Created by luardo on 16/10/16.
 */
import dispatcher from "../dispatcher"

export function addToFavorites(address) {
    dispatcher.dispatch({
        type: "ADD_LOCATION",
        address
    });
}

export function removeFavorite(address) {
    dispatcher.dispatch({
        type: "REMOVE_LOCATION",
        address
    });
}