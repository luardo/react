import dispatcher from "../dispatcher"

export function createPost(title) {
    dispatcher.dispatch({
        type: "CREATE_POST",
        title
    });
}

export function deletePost(id) {
    dispatcher.dispatch({
        type: "DELETE_POST",
        id
    });
}