/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import About from "./components/Pages/About";
import Featured from "./components/Pages/Featured";
import Layout from "./components/Pages/Layout";
import Single from "./components/Pages/Single";
import Places from "./components/Pages/Places";



const app = document.getElementById('app');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={Featured}></IndexRoute>
            <Route path="single" name="single" component={Single}></Route>
            <Route path="about" name="about" component={About}></Route>
            <Route path="places" name="places" component={Places}></Route>
        </Route>
    </Router>,
    app);
