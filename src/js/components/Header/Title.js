/**
 * Created by luardo on 18/08/16.
 */

import React from "react";

export default class Title extends React.Component {
    render() {
        return (
            <h1>{this.props.title}</h1>
        );
    }
}
