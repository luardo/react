/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import { IndexLink, Link } from "react-router";

export default class Banner extends React.Component {
    handleChange(e) {
        const title = e.target.value;
        this.props.changeTitle(title);
    }

    render() {
        const {title} = this.props;
        const {subtitle} = this.props;
        const {backgroundImage} = this.props;

        const containerStyle = {
            backgroundImage: 'url(../../../img/' + backgroundImage + ')'
        };

        return (
            <header class="intro-header" style={containerStyle}>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            <div class="post-heading">
                                <h1>{title} </h1>
                                <h2 class="subheading">{subtitle}</h2>
                                <span class="meta">Posted by
                                    <Link to="/">Start Bootstrap</Link>
                                    on August 24, 2014</span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}
