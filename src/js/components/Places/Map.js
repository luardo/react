/**
 * Created by luardo on 14/10/16.
 */
import React from 'react';

export default class Map extends React.Component {

    componentDidMount() {

        this.componentDidUpdate();
    }

    componentDidUpdate() {

        if(this.lastLat == this.props.lat && this.lastLng == this.props.lng) {

            return;
        }

        this.lastLat = this.props.lat;
        this.lastLng = this.props.lng;

        var map = new GMaps({
            el: '#map',
            lat: this.props.lat,
            lng: this.props.lng
        });

        // Adding a marker to the location we are showing

        map.addMarker({
            lat: this.props.lat,
            lng: this.props.lng
        });

    }

    render(){

        return (
            <div className="map-holder">
                <p>Loading...</p>
                <div id="map"></div>
            </div>
        );
    }

}