/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import Post from "../../components/Pages/Post"
import { Link } from "react-router";

import Banner from "../../components/Header/Banner";
import * as PostActions from "../../actions/PostActions";
import PostStore from "../../Stores/PostStore"




export default class Featured extends React.Component {

    constructor() {
        super();
        this.getPosts = this.getPosts.bind(this);
        this.state = {
            posts: PostStore.getAll()
        }
    }

    componentWillMount() {
        PostStore.on("change", this.getPosts);
    }

    componentWillUnmount() {
        PostStore.removeListener("change", this.getPosts);
    }

    getPosts() {
        this.setState({
            posts: PostStore.getAll()
        });
    }

    createPost() {
        PostActions.createPost(Date.now());
    }

    render() {
        const containerStyle = {
            backgroundImage: 'url(../../../img/home-bg.jpg)'
        };
        const {posts} = this.state;


        const PostComponent = posts.map((post) => {
            return <Post key={post.id} {...post} />
        });

        return (
            <div>

                <Banner title="Clean Blog" subtitle="A Clean Blog Theme by Start Bootstrap"
                    backgroundImage="home-bg.jpg"></Banner>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        {PostComponent}
                        </div>
                    </div>
                </div>
                <button class="text-center btn btn-primary" onClick={this.createPost.bind(this)}>Add new Post </button>
            </div>

        );
    }
}
