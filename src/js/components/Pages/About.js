/**
 * Created by luardo on 10/09/16.
 */
/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import Banner from "../../components/Header/Banner";


export default class About extends React.Component {
    render() {

        const containerStyle = {
            backgroundImage: 'url(../../../img/about-bg.jpg)'
        };

        return (
            <div>
                <Banner title="About me" subtitle="This is what I do." backgroundImage="about-bg.jpg"></Banner>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe nostrum ullam eveniet pariatur voluptates odit, fuga atque ea nobis sit soluta odio, adipisci quas excepturi maxime quae totam ducimus consectetur</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius praesentium recusandae illo eaque architecto error, repellendus iusto reprehenderit, doloribus, minus sunt. Numquam at quae voluptatum in officia voluptas voluptatibus, minus!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum molestiae debitis nobis, quod sapiente qui voluptatum, placeat magni repudiandae accusantium fugit quas labore non rerum possimus, corrupti enim modi! Et.</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
