/**
 * Created by luardo on 14/10/16.
 */

import React from "react";
import Map from "../Places/Map"
import Banner from "../Header/Banner"
import CurrentLocation from "../Places/CurrentLocation"
import LocationList from "../Places/LocationList"
import Search from "../Places/Search"


import * as PlacesActions from "../../actions/PlacesActions";
import PlacesStore from "../../Stores/PlacesStore"


export default class Places extends React.Component  {

    constructor() {
        super();

        this.state = {
            favorites: PlacesStore.getAll(),
            currentAddress: 'Paris, France',
            mapCoordinates: {
                lat: 48.856614,
                lng: 2.3522219
            }
        }
    }

    toggleFavorite(address) {
        if(this.isAddressInFavorites(address)) {
            this.removeFromFavorites(address);
        } else {
            this.addToFavorites(address);
        }
    }

    addToFavorites(address) {
        PlacesStore.add(address);
        this.setState({
            favorites: PlacesStore.getAll()
        });
    }

    removeFromFavorites(address){
        PlacesStore.remove(address);
        this.setState({
            favorites: PlacesStore.getAll()
        });
    }

    isAddressInFavorites(address) {
        var favorites = this.state.favorites;

        for(var i = 0; i < favorites.length; i++){

            if(favorites[i].address == address){
                return true;
            }

        }

        return false;
    }



    searchForAddress(address) {

        var self = this;

        // We will use GMaps' geocode functionality,
        // which is built on top of the Google Maps API

        GMaps.geocode({
            address: address,
            callback: function(results, status) {

                if (status !== 'OK') return;

                var latlng = results[0].geometry.location;

                self.setState({
                    currentAddress: results[0].formatted_address,
                    mapCoordinates: {
                        lat: latlng.lat(),
                        lng: latlng.lng()
                    }
                });

            }
        });

    }

    render() {

        return (

            <div>
                <Banner title="Places" subtitle="this are my favorite places in the world" backgroundImage="about-bg.jpg"></Banner>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            <h1>Your Google Maps Locations</h1>

                            <Search onSearch={this.searchForAddress.bind(this)} />

                            <Map lat={this.state.mapCoordinates.lat} lng={this.state.mapCoordinates.lng} />

                            <CurrentLocation address={this.state.currentAddress}
                                favorite={this.isAddressInFavorites(this.state.currentAddress)}
                                onFavoriteToggle={this.toggleFavorite.bind(this)} />

                            <LocationList locations={this.state.favorites} activeLocationAddress={this.state.currentAddress}
                                onClick={this.searchForAddress.bind(this)} />

                        </div>
                    </div>
                </div>
            </div>

        );
    }

}