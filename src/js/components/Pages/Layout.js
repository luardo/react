/**
 * Created by luardo on 18/08/16.
 */

import React from "react";
import { Link } from "react-router";
import Nav from "../../components/Header/Nav"


export default class Layout extends React.Component {
    render() {
        return (
            <div>
                <Nav></Nav>
            {this.props.children}
            </div>

        );
    }
}
