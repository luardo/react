/**
 * Created by luardo on 11/09/16.
 */
/**
 * Created by luardo on 18/08/16.
 */

import React from "react";

export default class Article extends React.Component {
    render() {

        const {title} = this.props;

        return (
        <div class="post-preview">
            <a href="post.html">
                <h2 class="post-title">
                {title}
                </h2>
                <h3 class="post-subtitle">
                    Problems look mighty small from 150 miles up
                </h3>
            </a>
            <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 18, 2014</p>
        </div>
        );
    }
}
